package ru.arubtsova.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(@Nullable final String value) {
        super("Error! Entered value ``" + value + "`` is not a number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
