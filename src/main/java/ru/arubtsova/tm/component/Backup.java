package ru.arubtsova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.service.IPropertyService;
import ru.arubtsova.tm.bootstrap.Bootstrap;

public class Backup extends Thread {

    @NotNull
    private static final String SAVE_COMMAND = "backup-save";

    @NotNull
    private static final String LOAD_COMMAND = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(propertyService.getBackupInterval());
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(SAVE_COMMAND);
    }

    public void load() {
        bootstrap.parseCommand(LOAD_COMMAND);
    }

}
