package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "create new task.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task Create:");
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskService().add(userId, name, description);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
