package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IBusinessService;
import ru.arubtsova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(@Nullable String userId, @Nullable String name, @Nullable String description);

}
